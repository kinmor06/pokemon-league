# Pokemon League

Stong Pokémon, weak Pokémon, that is only the foolish perception of people. Truly skilled trainers should try to win with their favorites.

### Getting Started

```
$ npm install
$ npm start
```

### Running Tests

This React app currently has no tests.

### Future Improvements

* Add Jest and Cypress
* use `localStorage` to make searching for Pokemons easier as the API does not have filtering capability
* Use `axios-extensions` for caching requests
