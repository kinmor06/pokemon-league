import axios from 'axios';

const API_BASE_URL = 'https://pokeapi.co/api/v2'

export const FETCH_LIST = 'FETCH_LIST';
export const FETCH_MONSTER_INFO = 'FETCH_MONSTER_INFO';
export const ADDED_TO_LINEUP = 'ADDED_TO_LINEUP';
export const REMOVE_ON_LINEUP = 'REMOVE_ON_LINEUP';

export function getPokedexList(url = null) {
  const list_url = url ? url : `${API_BASE_URL}/pokemon/?limit=100`
  const request = axios.get(list_url);

  return {
    type: FETCH_LIST,
    payload: request
  }
}

export function getPokemonInformation(name) {
  const pokemon_url = `${API_BASE_URL}/pokemon/${name}/`;
  const request = axios.get(pokemon_url);

  return {
    type: FETCH_MONSTER_INFO,
    payload: request
  };
}

export function addPokemonToLineup(pokemon) {
  return {
    type: ADDED_TO_LINEUP,
    payload: {
      name: pokemon.name,
      url: pokemon.url,
      id: pokemon.id || pokemon.url.split('/')[6] // Get the ID based on the API URL
    }
  }
}

export function removePokemonFromLineup(name) {
  return {
    type: REMOVE_ON_LINEUP,
    payload: name
  }
}
