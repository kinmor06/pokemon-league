import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getPokemonInformation, addPokemonToLineup, getPokedexList } from '../actions/index';
import { bindActionCreators } from 'redux';
import { capitalize, titleize } from '../functions/index';

import LineupActionButton from './lineup_action_button';

class PokedexList extends Component {
  constructor(props) {
    super(props);

    this.props.getPokedexList();

    this.renderItem = this.renderItem.bind(this);
  }

  renderItem(pokemon) {
    const name = pokemon.name;

    return (
      <li key={name} className="list-group-item clearfix">
        <button
          onClick={() => this.props.getPokemonInformation(name)}
          className="btn btn-link btn-sm">
          {capitalize(name)}
        </button>
        <span className="float-right">
          <LineupActionButton pokemon={pokemon} />
        </span>
      </li>
    );
  }

  render() {
    const pokedexList = this.props.pokedexList;

    if (!pokedexList.results) {
      return (
        <div className="pokedex">
          <h4>
            &nbsp;&nbsp;Pokédex
          </h4>
          <ul className="list-group">
            <li className="list-group-item">Initializing...</li>
          </ul>
        </div>
      );
    }

    return (
      <div className="pokedex">
        <h4>
          &nbsp;&nbsp;Pokédex
        </h4>
        <ul className="list-group">
          {pokedexList.results.map(this.renderItem)}
        </ul>
        <nav className="clearfix">
          <ul className="pagination float-right">
            <li className="page-item">
              <a className="page-link btn-danger" onClick={() => { return this.props.getPokedexList(pokedexList.previous)} }>Previous</a>
            </li>
            <li className="page-item">
              <a className="page-link" onClick={() => { return this.props.getPokedexList(pokedexList.next)} }>Next</a>
            </li>
          </ul>
        </nav>
      </div>
    );
  }
}

function mapStateToProps({ pokedexList, pokemonLineup }) {
  return { pokedexList, pokemonLineup };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getPokemonInformation: getPokemonInformation,
      addPokemonToLineup: addPokemonToLineup,
      getPokedexList: getPokedexList
    },
    dispatch
  )
}


export default connect(mapStateToProps, mapDispatchToProps)(PokedexList);
