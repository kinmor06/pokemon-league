import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { addPokemonToLineup, removePokemonFromLineup } from '../actions/index';
import { capitalize, titleize } from '../functions/index';

class LineUpActionButton extends Component {
  constructor(props) {
    super(props);

    this.pokemonIsOnLineup = this.pokemonIsOnLineup.bind(this);
    this.validateAddition  = this.validateAddition.bind(this);
  }

  pokemonIsOnLineup(name) {
    return this.props.pokemonLineup.map((pokemon) => { return pokemon.name }).includes(name);
  }

  validateAddition(pokemon) {
    const currentLineup = this.props.pokemonLineup;

    if ( this.pokemonIsOnLineup(pokemon.name) ) {
      return alert(`${capitalize(pokemon.name)} is already part of your team.`);
    }

    if ( currentLineup.length >= 6 ) {
      return alert('You can only have 6 pokemon on your team.');
    }

    return this.props.addPokemonToLineup(pokemon);
  }


  render() {
    const pokemon = this.props.pokemon;
    return (
      <button
        onClick={() => this.validateAddition(pokemon) }
        className={"btn btn-sm " + (this.pokemonIsOnLineup(pokemon.name) ? 'btn-danger' : 'btn-outline-danger')}>
        {this.pokemonIsOnLineup(pokemon.name) ? 'Added' : 'Add to Lineup'}
      </button>
    );
  }
}


function mapStateToProps({ pokemonLineup }) {
  return { pokemonLineup };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      addPokemonToLineup: addPokemonToLineup,
      removePokemonFromLineup: removePokemonFromLineup
    },
    dispatch
  )
}


export default connect(mapStateToProps, mapDispatchToProps)(LineUpActionButton);
