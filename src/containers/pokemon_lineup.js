import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { getPokemonInformation, removePokemonFromLineup } from '../actions/index';
import { capitalize, titleize } from '../functions/index';
import EmptyCard from '../components/empty_card.js'

class PokemonLineup extends Component {
  constructor(props) {
    super(props);

    this.renderCard = this.renderCard.bind(this);
  }

  renderCard(pokemon) {
    return (
      <div className="col-4" key={pokemon.name}>
        <div className="lineup card card-inverse text-center">
          <img className="card-img-top mx-auto d-block pokemon-img" src={`/images/pokemon/${pokemon.id}.png`} />
          <div className="card-img-overlay">
            <div className="row float-right">
              <a onClick={() => this.props.removePokemonFromLineup(pokemon.name)} className="card-link">
                <i className="fas fa-times-circle text-danger"></i>
              </a>
            </div>
          </div>
          <div className="card-body">
            <h5 className="card-title">
              {capitalize(pokemon.name)}
            </h5>
          </div>
        </div>
      </div>
    );
  }

  renderEmptyCards(lineup_size) {
    return (
      Array(6 - lineup_size).fill('').map((item, index) => {
        return <EmptyCard key={index} />
      })
    );
  }

  render() {
    return (
      <div>
        <div className="row lineup-container">
          {this.props.lineup.map(this.renderCard)}
          {this.renderEmptyCards(this.props.lineup.length)}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { lineup: state.pokemonLineup };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ removePokemonFromLineup }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(PokemonLineup);
