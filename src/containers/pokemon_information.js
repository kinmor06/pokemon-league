import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { capitalize, titleize } from '../functions/index';

import LineupActionButton from './lineup_action_button';

class PokemonInformation extends Component {
  renderStats(stat) {
    return (
      <li key={stat.stat.name}>
        <strong>{titleize(stat.stat.name)}: </strong>{stat.base_stat}<br/>
      </li>
    );
  }

  renderAbility(ability) {
    return (
      <li key={ability.ability.name}>
        <strong>{titleize(ability.ability.name)}: </strong>{ ability.is_hidden ? '(Hidden)' : ''}
      </li>
    );
  }

  renderMove(move) {
    return (
      <li key={move.move.name}>
        {titleize(move.move.name)}
      </li>
    );
  }

  render() {
    if (!this.props.pokemon) {
      return (
        <div className="card poke-information">
          <div className="card-body">
            Select a pokemon.
          </div>
        </div>
      );
    }

    const pokemon = this.props.pokemon;

    return (
      <div className="card poke-information">
        <div className="card-body">
          <div className="row">
            <div className="col text-center">
              <img className="mx-auto d-block" src={`/images/pokemon/${pokemon.id}.png`} />
              <h5>{capitalize(pokemon.name)}</h5>
              <div>
                <LineupActionButton pokemon={pokemon} />
              </div>
            </div>
            <div className="col">
              <table className="table">
                <tbody>
                  <tr>
                    <th>Type</th>
                    <td>{pokemon.types.map((type) => { return type.type.name }).join(', ')}</td>
                  </tr>
                  <tr>
                    <th>Height</th>
                    <td>{pokemon.height / 10} m</td>
                  </tr>
                  <tr>
                    <th>Weight</th>
                    <td>{pokemon.weight / 10} kg</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div className="row">
            <div className="col">
              <strong>Base Stats</strong>
              <ul>
                {pokemon.stats.map(this.renderStats)}
              </ul>
            </div>
            <div className="col">
              <strong>Abilities</strong>
              <ul>
                {pokemon.abilities.map(this.renderAbility)}
              </ul>
            </div>
          </div>
          <div className="row">
            <div className="col">
              <strong>Moves</strong>
              <ul>
                {pokemon.moves.map(this.renderMove)}
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { pokemon: state.activePokemon };
}

export default connect(mapStateToProps)(PokemonInformation);
