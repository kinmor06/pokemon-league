import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchPokemon } from '../actions/index';

class SearchBar extends Component {
  constructor(props) {
    super(props);

    this.state = { query: '' };

    this.onInputChange = this.onInputChange.bind(this);
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  onInputChange(event) {
    this.setState({ query: event.target.value });
  }


  onFormSubmit(event) {
    event.preventDefault();

    this.props.fetchPokemon(this.state.query);
    this.setState({ query: '' });
  }

  render() {
    return (
      <form onSubmit={this.onFormSubmit} className="input-group">
        <input
          placeholder="Find Pokemon"
          className="form-control"
          value={this.state.quert}
          onChange={this.onInputChange}
          />
        <span className="input-group-btn">
          <button type="submit" className="btn btn-secondary">Search</button>
        </span>
      </form>
    );
  }
}


function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchPokemon }, dispatch);
}

export default connect(null, mapDispatchToProps)(SearchBar);
