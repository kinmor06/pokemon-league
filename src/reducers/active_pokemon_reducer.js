import { FETCH_MONSTER_INFO } from '../actions/index';

export default function(state = null, action) {
  switch (action.type) {
  case FETCH_MONSTER_INFO:
    return action.payload.data;
  }

  return state;
}
