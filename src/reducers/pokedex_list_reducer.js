import { FETCH_LIST } from '../actions/index';

export default function(state = {}, action) {
  switch (action.type) {
  case FETCH_LIST:
    const response_data = action.payload.data;

    return {
      previous: response_data.previous,
      next: response_data.next,
      results: response_data.results
    };
  };

  return state;
}
