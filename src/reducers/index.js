import { combineReducers } from 'redux';

import PokedexListReducer from './pokedex_list_reducer';
import ActivePokemonReducer from './active_pokemon_reducer';
import PokemonLineupReducer  from './pokemon_lineup_reducer';

const rootReducer = combineReducers({
  pokedexList: PokedexListReducer,
  activePokemon: ActivePokemonReducer,
  pokemonLineup: PokemonLineupReducer
});

export default rootReducer;
