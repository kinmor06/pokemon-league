import { ADDED_TO_LINEUP, REMOVE_ON_LINEUP } from '../actions/index';

export default function(state = [], action) {
  switch (action.type) {
  case ADDED_TO_LINEUP:
    return [...state, action.payload];
  case REMOVE_ON_LINEUP:
    return state.filter((pokemon) => { return pokemon.name != action.payload });
  }

  return state;
}
