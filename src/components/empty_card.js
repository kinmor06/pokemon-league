import React from 'react';

export default (props) => {
  return (
    <div className="col-4" key={props.key}>
      <div className="lineup card empty text-center">
        <img className="card-img-top mx-auto d-block" src="/images/pokemon/question-mark.png" />
        <div className="card-body">
          <p className="card-text"></p>
        </div>
      </div>
    </div>
  );
}
