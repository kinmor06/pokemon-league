import React, { Component } from 'react';

import PokedexList from '../containers/pokedex_list';
import PokemonInformation from '../containers/pokemon_information';
import PokemonLineup from '../containers/pokemon_lineup';

export default class App extends Component {
  render() {
    return (
      <div className="row">
        <div className="col-8 lineup-col">
          <img src="/images/pokeball.png" className="pokeball top"  />
          <img src="/images/pokeball.png" className="pokeball bottom"/>
          <PokemonLineup />
          <PokemonInformation />
        </div>
        <div className="col pokedex-col">
          <PokedexList />
        </div>
      </div>
    );
  }
}
