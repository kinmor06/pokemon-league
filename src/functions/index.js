export function capitalize(str){
  return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase();
}

export function titleize(sentence) {
  if(!sentence.split) return sentence;
  var _titleizeWord = function(string) {
    return capitalize(string);
  },
  result = [];
  sentence.split(/[.,\/ -]/).forEach(function(w) {
      result.push(_titleizeWord(w));
  });
  return result.join(" ");
}
